let mongoose = require("mongoose");
let User = require("../models/user");

let CommentSchema = new mongoose.Schema({
    text: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: User
        },
        username: String
    }
});

module.exports = new mongoose.model("Comment", CommentSchema);