let mongoose = require("mongoose");
let Comment = require("../models/comments");
let User = require("../models/user");

//Schema Setup
let CampgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String,
    price: Number,
    createdAt: {
        type: Date,
        default: Date.now
    },
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comment"
    }]
});

module.exports = mongoose.model("Campground", CampgroundSchema);