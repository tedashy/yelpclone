let mongoose = require("mongoose");
let Campground = require("./models/campgrounds");
let Comment = require("./models/comments");

var data = [{
        name: "Clouds Rest",
        image: "https://images.unsplash.com/photo-1497900304864-273dfb3aae33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
        description: "blah blah blah"
    },
    {
        name: "Mts Goat",
        image: "https://www.nps.gov/mora/planyourvisit/images/OhanaCampground2016_CMeleedy_01_web.jpeg?maxwidth=1200&maxheight=1200&autorotate=false",
        description: "blah blah blah"
    },
    {
        name: "Mississipi Pass",
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYGPlccK6KuXu7F_u5ssgzCHYsAOjJQRtkmMdT4GhMqH59fP2djQ",
        description: "blah blah blah"
    }
];

function seedDB() {
    Campground.deleteMany({}, function (err, deleted) {
        if (err) {
            console.log("Error deleting campgrounds");
            // } else {
            //     console.log("DELETED CAMPGROUDS!");
            //     data.forEach(function (camp) {
            //         Campground.create(camp, function (err, campground) {
            //             if (err) {
            //                 console.log("Error creating new campground!");
            //             } else {
            //                 console.log("Created Campground");
            //                 Comment.create({
            //                     author: "Tenashy",
            //                     text: "Generic comment"
            //                 }, function (err, comment) {
            //                     if (err) {
            //                         console.log("Error creating comment");
            //                     } else {
            //                         console.log("Created Comment");
            //                         campground.comments.push(comment);
            //                         campground.save();
            //                     }
            //                 });
            //             }
            //         });
            //     });
        }
    });
}


module.exports = seedDB;