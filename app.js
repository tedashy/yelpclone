const express = require("express");
let app = express();
let bodyParser = require("body-parser");
let mongoose = require("mongoose");
let flash = require("connect-flash");
let methodOverride = require("method-override");
let passport = require("passport");
let LocalStrategy = require("passport-local");
let LocalStrategyMongoose = require("passport-local-mongoose");
let User = require("./models/user");
let port = 8080;
let seedDB = require("./seeds");

//seedDB(); //seed the database

mongoose.connect("mongodb://localhost:27017/yelpcamp", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(methodOverride("_method"));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(flash());

//Express-session config
app.use(
  require("express-session")({
    secret: "we all need some validation :(",
    saveUninitialized: false,
    resave: false
  })
);

//Passport config
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Middleware to parse req.user to all routes
app.use(function (req, res, next) {
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});

//Routes
let commentRoutes = require("./routes/comments"),
  campgroundsRoutes = require("./routes/campgrounds"),
  indexRoutes = require("./routes/index");

//Router config
app.use("/", indexRoutes);
app.use("/campgrounds", campgroundsRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);

//listening to routes on the local server
app.listen(port, () => console.log("App listening on port " + port));