let express = require("express");
let router = express.Router({
    mergeParams: true
});
let Campground = require("../models/campgrounds");
let Comment = require("../models/comments");
let middleware = require("../middleware");

//Comment Routes

//Comments New
router.get("/new", middleware.isLoggedIn, function (req, res) {
    Campground.findById(req.params.id, function (err, foundCampground) {
        if (err) {
            console.log("Error finding campground to comment on :" + err);
        } else {
            res.render("comments/new", {
                campground: foundCampground
            });
        }
    });
});

//Comments Create
router.post("/", middleware.isLoggedIn, function (req, res) {
    Campground.findById(req.params.id, function (err, foundCampground) {
        if (err) {
            console.log("Error finding campground to comment on :" + err);
        } else {
            let comment = req.body.comment;
            Comment.create(comment, function (err, createdComment) {
                if (err) {
                    req.flash("error", "Something went wrong :(");
                    console.log("Error creating comment :" + err);
                } else {
                    //add username and id to comment
                    createdComment.author.id = req.user._id;
                    createdComment.author.username = req.user.username;
                    console.log(createdComment);
                    //save comment
                    createdComment.save();
                    foundCampground.comments.push(createdComment);
                    foundCampground.save(function (err, data) {
                        if (err) {
                            console.log(err);
                        } else {
                            req.flash("success", "Successfully added comment");
                            console.log(data);
                            res.redirect("/campgrounds/" + foundCampground.id);
                        }
                    });
                }
            });
        }
    });
});

//edit comment route
router.get("/:comment_id/edit", middleware.checkCommentOwnership, (req, res) => {
    Campground.findById(req.params.id, (err, foundCampground) => {
        if (err) {
            res.redirect("back");
        } else {
            Comment.findById(req.params.comment_id, (err, foundComment) => {
                if (err) {
                    res.redirect("back");
                } else {
                    res.render("comments/edit", {
                        campground: foundCampground,
                        comment: foundComment
                    });
                }
            });
        }
    });
});

//update comment route
router.put("/:comment_id", middleware.checkCommentOwnership, (req, res) => {
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, updatedComment) => {
        if (err) {
            res.redirect("back");
        } else {
            req.flash("success", "Comment updated");
            res.redirect("/campgrounds/" + req.params.id);
        }
    });
});

//Comments Destroy Route
router.delete("/:comment_id", middleware.checkCommentOwnership, (req, res) => {
    Comment.findByIdAndRemove(req.params.comment_id, (err, deletedComment) => {
        if (err) {
            res.redirect("back");
        } else {
            req.flash("success", "Comment deleted");
            res.redirect("/campgrounds/" + req.params.id);
            console.log(deletedComment);
        }
    });
});

module.exports = router;