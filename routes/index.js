let express = require("express");
let router = express.Router();
let passport = require("passport");
let User = require("../models/user");

//Root Route
router.get("/", function (req, res) {
    res.render("landing");
});

//=========================== Auth Routes ===============================
//register form route
router.get("/signup", function (req, res) {
    res.render("register");
});

//handling signup logic
router.post("/signup", function (req, res) {
    User.register(
        new User({
            username: req.body.username
        }),
        req.body.password,
        function (err, newUser) {
            if (err) {
                req.flash("error", err.message);
                console.log("Error creating a new user :" + err);
                return res.redirect("/signup");
            }
            passport.authenticate("local")(req, res, function () {
                req.flash("success", "Welcome to YelpCamp " + newUser.username);
                res.redirect("/campgrounds");
            });
        }
    );
});

//Login Routes
//login form route
router.get("/login", function (req, res) {
    res.render("login");
});

//handling login logic
router.post(
    "/login",
    passport.authenticate("local", {
        successRedirect: "/campgrounds",
        failureRedirect: "/login"
    }),
    function (req, res) {}
);

//Logout Route
router.get("/logout", function (req, res) {
    req.flash("success", "Logged you out!");
    req.logout();
    res.redirect("/login");
});

module.exports = router;