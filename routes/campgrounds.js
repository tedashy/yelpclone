let express = require("express");
let router = express.Router();
let Campground = require("../models/campgrounds");
let middleware = require("../middleware");

//Index Route
router.get("/", function (req, res) {
  //Get all campgrounds from DB
  Campground.find({}, function (err, campgrounds) {
    if (err) {
      console.log("error :" + error);
    } else {
      res.render("campgrounds/index", {
        campgrounds: campgrounds
      });
    }
  });
});

//New Route
router.get("/new", middleware.isLoggedIn, function (req, res) {
  res.render("campgrounds/new");
});

//Create Route
router.post("/", middleware.isLoggedIn, function (req, res) {
  let name = req.body.name;
  let image = req.body.image;
  let desc = req.body.description;
  let price = parseFloat(req.body.price);
  let author = {
    id: req.user._id,
    username: req.user.username
  };

  let newCampground = {
    name: name,
    image: image,
    description: desc,
    price: price,
    author: author
  };

  //Create a new campground and save to DB
  Campground.create(newCampground, function (err, newlyCreated) {
    if (err) {
      console.log("error :" + error);
    } else {
      res.redirect("/campgrounds");
    }
  });
});

//Show
router.get("/:id", function (req, res) {
  //find the campground with provided ID
  Campground.findById(req.params.id)
    .populate("comments")
    .exec(function (err, foundCampground) {
      if (err) {
        console.log("error :" + err);
      } else {

        //// Added this block, to check if foundCampground exists, and if it doesn't to throw an error via connect-flash and send us back to the homepage
        if (!foundCampground) {
          req.flash("error", "Item not found");
          return res.redirect("back");
        }

        //render the template with that campground
        res.render("campgrounds/show", {
          campground: foundCampground
        });
      }
    });
});

//Edit Route
router.get("/:id/edit", middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findById(req.params.id, (err, foundCampground) => {
    if (err) {
      console.log("Error finding campground to edit: " + err);
    } else {

      //// Added this block, to check if foundCampground exists, and if it doesn't to throw an error via connect-flash and send us back to the homepage
      if (!foundCampground) {
        req.flash("error", "Item not found");
        return res.redirect("back");
      }

      res.render("campgrounds/edit", {
        campground: foundCampground
      });
    }
  });
});

//Update route
router.put("/:id", middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndUpdate(
    req.params.id,
    req.body.campground,
    (err, updatedCampground) => {
      if (err) {
        console.log(req.body.campground);
        console.log("Error udpating campground" + err);
      } else {
        res.redirect("/campgrounds/" + req.params.id);
        console.log(updatedCampground);
      }
    }
  );
});

//Destroy Route
router.delete("/:id", middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndDelete(req.params.id, (err, deletedCampground) => {
    if (err) {
      console.log("Error deleting campground with error: " + err);
    } else {

      console.log(
        "====================== deleted Campground ===================="
      );
      console.log(deletedCampground);
      console.log("========================== end =========================");
      res.redirect("/campgrounds");
    }
  });
});

module.exports = router;